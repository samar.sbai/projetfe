import React, {useState} from 'react';
import {Table, Button, Modal} from 'react-bootstrap';
import "./nav.css"

function TarifLettre() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
      <div className='tarifL'>
       <div className='CTL'> 
         <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h5>Bienvenue sur l'Assistent de la Poste</h5></Modal.Title>
        </Modal.Header>
        <Modal.Body>
         <h5>Tarifs des lettres du régime intérieur</h5> 
         <Table striped bordered hover>
         <thead>
          <tr><th>#</th> <th>Prix</th>  </tr>
        </thead>
        <tbody>
        <tr><td>Lettre jusqu’à 20 gr</td><td>1,00 DT</td></tr>
        <tr><td>Lettre jusqu’à 100 gr </td><td>1,200 DT</td></tr>
        <tr><td>Lettre jusqu’à 250 gr</td><td>1,500 DT</td></tr>
        <tr><td>Lettre Supérieur à 250 jusqu’à 500 gr</td><td>2,000 DT</td></tr>
        <tr><td>Lettre Supérieur à 500 jusqu’à 1000 gr</td><td>2,500 DT</td></tr>
        <tr><td>Lettre Supérieur à 1000 jusqu’à 2000 gr</td><td>3,500 DT</td></tr>
         </tbody> 
       </Table> 
          
        <h5>Tarifs des lettres du régime international</h5>
        <Table striped bordered hover>
        <thead>
          <tr> <th>#</th> <th>UMA</th>  <th>Pays Arabes</th> <th>Reste du monde</th></tr>
        </thead>
        <tbody>
          <tr><td>Carte postale</td><td>1,200 Dt</td><td>1,200 DT</td><td>1,500 DT</td></tr>
          <tr><td>Lettre jusqu’à 20 gr</td><td>0,750 DT</td><td>2,000 DT</td><td>2,000 DT</td> </tr>
          <tr><td>Lettresupérieur à 20 jusqu’à 100 gr</td><td>1.500 DT</td><td>3,000 DT</td><td>3,000 DT</td></tr>
          <tr><td>Lettre supérieur à 100 jusqu’à 250 gr</td><td>2.500 DT</td><td>5,000 DT</td><td>6,000 DT</td></tr>
          <tr><td>Lettre supérieur à 250 jusqu’à 500 gr</td><td>4.000 DT</td><td>10,000 DT</td><td>12,000 DT</td></tr>
          <tr><td>Lettre supérieur à 500 jusqu’à 1000 gr</td><td>6.000 DT</td><td>15,000 DT</td><td>18,000 DT</td></tr>
          <tr><td>Lettre supérieur à 1000 jusqu’à 2000 gr</td><td>12.000 DT</td><td>20,000 DT</td><td>22,000 DT</td></tr>
       </tbody>
        </Table>
        </Modal.Body>
       <Modal.Footer>
       <Button variant="secondary" onClick={handleClose}>Close</Button> 
       </Modal.Footer>
       </Modal>
       <Button variant="light"onClick={handleShow}>Les tarifs des lettres</Button>

       </div> 
      </div>
      
    
  );
}

export default TarifLettre;



