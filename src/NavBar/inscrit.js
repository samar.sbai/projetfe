import React, {useContext, useRef, useState} from 'react';
import {Form, Row, Col, Button, Modal} from 'react-bootstrap';


function Inscrit() {

 //fent
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
   <div className='inscrit'> 
       
      <Modal show={show} onHide={handleClose} variant="primary" >
        <Modal.Header closeButton>
        <Modal.Title><h5>Bienvenue sur la poste tunisienne</h5></Modal.Title>
        </Modal.Header>
        <Modal.Body>

  <Form>
    <Row className="mb-3">
    <Form.Group as={Col} controlId="nom" >
    <Form.Label>Nom:</Form.Label>
    <Form.Control />
    </Form.Group>
    <Form.Group as={Col} controlId="prenom">
    <Form.Label>Prénom :</Form.Label>
    <Form.Control />
    </Form.Group>
    </Row>



    <Row className="mb-3">
    <Form.Group as={Col} controlId="email">
    <Form.Label>Email :</Form.Label>
    <Form.Control type="Email" placeholder="XXX@EXEMPLE.com" />
    </Form.Group>

    <Form.Group as={Col} controlId="mdp" >
    <Form.Label>Votre mot passe :</Form.Label>
    <Form.Control type="password" placeholder="mot passe" />
    </Form.Group>
    </Row>


   <Form.Group className="mb-3" controlId="adr">
   <Form.Label>Address :</Form.Label>
   <Form.Control placeholder="1234 xxxx " />
   </Form.Group>

   <Form.Group className="mb-3" controlId="codep">
   <Form.Label>Code postale :</Form.Label>
   <Form.Control placeholder="code postale" />
   </Form.Group>

  <Row className="mb-3"> 
  <Form.Group className="mb-3" id="H">
  <Form.Check type="checkbox" label="Homme" />
  </Form.Group>
  <Form.Group className="mb-3" id="F">
  <Form.Check type="checkbox" label="Femme" />
  </Form.Group>
  </Row>
  
  
  <Form.Group controlId="cin" className="mb-3">
  <Form.Label>Image du carte d'identité :</Form.Label>
  <Form.Control type="file" />
  </Form.Group>



<Button variant="primary" type="submit"> 
S'inscrire </Button>
</Form>
    
</Modal.Body>

  <Modal.Footer>
   <Button variant="secondary" onClick={handleClose}>Close</Button> 
  </Modal.Footer>
 </Modal>

  <Button variant="light" onClick={handleShow} >S'inscrire</Button> 
   </div>

    
  );
}

export default Inscrit;
