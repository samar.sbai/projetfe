import  React,{useState} from 'react' 
import {Link} from 'react-router-dom'
import {Card, Modal,Button,Row,Col} from 'react-bootstrap'
import CréerL from './créerLettre';
import ConsulterL from "./consulterLettre";
import ModifierLettre from './ModifierLettre';
import SupprimerLettre from './SupprimerLettre';
import './E-lettre.css';
import NavLettre from './achat'; 
  function Contenu() {
    return (
   <article className='contenu'>   
   <h3>Les types des lettres </h3> 

   <section className='types'>  
      <div className='lettreORD'>   
      <Card border="primary" style={{ width: '18rem' }} bg="cornsilk">
      <Card.Body>
      <Card.Title className="title">Lettre ordinaire</Card.Title>
      <Card.Text>
      <p>Avantages</p>
        <ul> 
        <li>Simple.</li>
        <li>Délais de livraison raisonnables, en général dans les 72 heures qui suivent le  jour de dépôt.</li>
        <li>Prix économiques.</li>
        </ul>
       Poids maximum: 2 Kg  <br></br> 
      Lieu de dépôt:<br></br>
      Boites postales ou  En ligne.
    </Card.Text>
    </Card.Body>
   </Card> </div>  

   <div className='lettreREC'> 
   <Card border="primary" style={{ width: '18rem' }} bg='Light'>
   <Card.Body>
   <Card.Title className="title">Lettre recommandé</Card.Title>
   <Card.Text>
   La lettre recommandée confère à votre courrier une valeur juridique 
   et une possibilité de suivi de traçabilité.
  <p>Avantages</p>
  <ul>
    <li>Preuve de réception signée par le destinataire ou son mandataire</li>
    <li>Indemnisation en cas de perte, détérioration ou spoliation.</li>
  </ul>
  Poids maximum: 2 Kg
    </Card.Text>
    </Card.Body>
   </Card> </div>

   <div className='lettrePRI'> 
   <Card border="primary" style={{ width: '18rem' }}>
   <Card.Body>
   <Card.Title className="title">Lettre prioritaire</Card.Title>
   <Card.Text>
   Pour vos envois ordinaires jusqu'à 20 grammes, 
   la Poste Tunisienne vous propose le mode de traitement  prioritaire.
   <br></br><br></br>
   Avantages
   <ul> 
     <li>Simple</li>
     <li>Livraison rapide: Votre lettre sera distribuée dans les 48 heures qui suivent le jour de dépôt</li>
   </ul>
    </Card.Text>
    </Card.Body>
   </Card> 
   </div>
   </section>
   <br></br>  <br></br>




   <h3>Écrire une lettre</h3>
   <br></br>  <br></br>
    <br></br>  <br></br>
  <section className='CRUDlettre'> 

   <div className='Créer'>
      <CréerL/>
   </div>

   <div className="Consulter">
     <ConsulterL/>
    </div>

    <div className="Modifier">
       <ModifierLettre/>
    </div>

    <div className="Supprimer">
       <SupprimerLettre/>
    </div>
</section>






 <br></br><br></br><br></br> 
 <div className='Button'>
   <Link to="/service">  
    <Button variant="outline-primary">←Retour</Button>
   </Link> 

   <Link to="/navLettre">   
      <Button variant="outline-primary">Découvrez nos offres du moment</Button>
   </Link> 
 </div>
 </article> 


 
    )
}
export default Contenu;