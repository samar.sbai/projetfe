import  React from 'react'
import {Form, Row, Col, Button, Modal} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import AnnulerRéservation from './annulerRéservation';
import ModifierRéservation from './modifierRéservation';
import './rendeVous.css'
function RendezVous() {
    return (
         <div className='rendezvous'>
            <img src='./rdv.jpg' alt='RDV'/>
            <br></br><br></br>
            <Row className="mb-3">
            <Form.Group as={Col} controlId="mail">
            <Form.Label>Votre adresse email:</Form.Label>
            <Form.Control />
            </Form.Group>

            <Form.Group as={Col} controlId="ntel">
            <Form.Label>Numéro du télèphone:</Form.Label>
            <Form.Control />
            </Form.Group>
           </Row>
           <br></br>

           <div className='Date'> 
           <label for="start">chosir une date :</label>
           <input type="date" id="calend" 
           value="2022-07-22"
            min="2018-01-01" max="2050-12-31" size={50}></input>

         <Form.Group className="mb-3" id="matin">
         <Form.Check type="checkbox" label="Matin" />
         </Form.Group>  
         <Form.Group className="mb-3" id="midi">
         <Form.Check type="checkbox" label="Après midi" />
         </Form.Group></div>  
           
         <br></br>
         <div className='confirmer'> 
         <p>Confirmation demandée par  :</p>
         <Form.Group className="mb-3" id="mail">
         <Form.Check type="checkbox" label="Email" />
         </Form.Group>  <br></br>
         <Form.Group className="mb-3" id="midi">
         <Form.Check type="checkbox" label="Appel téléphonique" />
         </Form.Group>
         </div>
     
           <br></br><br></br>
         <div className='CRUDrendez-vous'>
             <div className='annuler'> 
              <AnnulerRéservation/> 
              </div> 
              <div className='modifier'> 
              <ModifierRéservation/>
              </div>      
         </div>




         <br></br><br></br>
         <div className='b'>
         <Link to="/service">  
          <Button variant="outline-primary">←Retour</Button>
         </Link>
         </div>
         <br></br><br></br> <br></br><br></br> <br></br><br></br>

     
</div>
    )
}
export default RendezVous;