import  React, {useState} from 'react'
import Consulter from './consulterProfile';
import Supprimer from './supprimerCompte';
import Modifier from './modifierProfil';
import {Link} from 'react-router-dom'
import {Form, Row, Col, Button} from 'react-bootstrap';
import "./CRUDprofile.css"
function Consultation() {
    return (
        <div className='gérerProfile'> 
            <img src='/profil.jpg' alt='profile'/>
            <br></br><br></br>
            <div className='CRUD'> 
             <Consulter/>
             <Supprimer/>
             <Modifier/>
             </div>

         
         
             <br></br><br></br>
         <div className='routour'> 
         <Link to="/service">  
          <Button variant="outline-primary">←Retour</Button>
         </Link> </div>
         
</div>
    )
}
export default Consultation;