import React,{useState} from 'react';
import {Table, Modal, Button} from 'react-bootstrap';


function AnnulerRéservation() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
    <div className='tarifC'>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h2>Annuler la réservation</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
      
       </Modal.Body>
       <form> 
         <label for="identifiant"> Adresse email</label>
         <input type="email" id="adremail"/>
         </form> 
       <Modal.Footer>
       <Button variant="primary">Annuler la réservation</Button> 
        <Button variant="secondary" onClick={handleClose}> Close </Button> 
         </Modal.Footer>
       </Modal>

 <Button variant="warning" onClick={handleShow}>Annuler</Button> 
    </div>

    
  );
}

export default AnnulerRéservation;