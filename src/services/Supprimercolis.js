import React,{useState} from 'react';
import {Table, Modal, Button} from 'react-bootstrap';


function SupprimerColis() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
    <div className='tarifC'>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h2>Supprimer vous coordonnées</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
      
       </Modal.Body>
       <form> 
         <label for="identifiant"> Code du votre colis </label>
         <input type="text" id="codecolis"/>
         </form> 
       <Modal.Footer>
       <Button variant="primary">Supprimer</Button> 
        <Button variant="secondary" onClick={handleClose}> Close </Button> 
         </Modal.Footer>
       </Modal>


    <Button variant="warning" onClick={handleShow}>Supprimer</Button> 
    </div>

    
  );
}

export default SupprimerColis;