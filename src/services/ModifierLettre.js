import React,{useState} from 'react';
import {Table, Modal, Button} from 'react-bootstrap';


function ModifierLettre() {
  const [show, setShow] = useState(false);
  const handleClose = () =>setShow(false);
  const handleShow = () =>setShow(true);
  return (
    <div className='tarifC'>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title><h2>Modifier</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
      
       </Modal.Body>

     <form> 

        <label for="identifiant"> Code de lettre </label>
         <input type="text" id="code"/>
         <br></br><br></br>

         <label for="destination">Nouvelle destination</label>
         <input type="text" id="dest"/>
         <br></br><br></br>

         <label for="code">Le code postale</label>
         <input type="text" id="codepostale"/>
         <br></br><br></br>
      </form>

       <Modal.Footer>
       <Button variant="primary">Modifier</Button> 
        <Button variant="secondary" onClick={handleClose}> Close </Button> 
         </Modal.Footer>
       </Modal>

     <Button variant="warning" onClick={handleShow}>Modifier</Button> 
    </div>

    
  );
}

export default ModifierLettre;