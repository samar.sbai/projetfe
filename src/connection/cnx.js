import React, {useState}  from 'react';
import {Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import './cnx.css';
function Cnx() {

  return (
    <div className='connexion'>
      <img src="/cnx.jpg" alt="c"/>
      <div className='Fcnx'>
  <Form >
  <Form.Group className="mb-3" controlId="emailC" >
  <Form.Label> adresse Email</Form.Label>
  <Form.Control  type="email" placeholder="xxx@Exemple.com" />
  <Form.Text  variant="danger" className="text-muted" >
  Nous ne partagerons jamais votre e-mail avec quelqu'un d'autre.
  </Form.Text>
  </Form.Group>

  <Form.Group className="mb-3" controlId="mdpC" >
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>
  <Form.Group className="mb-3" controlId="validation">
    <Form.Check type="checkbox" label="Vérifiez-moi" />
  </Form.Group>

<Link to="/service"> 
  <Button variant="primary" type="submit">Se connecté </Button>
</Link>

</Form>
</div>   
</div>);
}
//}
export default Cnx;