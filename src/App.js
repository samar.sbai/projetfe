import './App.css';
import Header from './headerPA/header';
import Lettre from './services/E-lettre';
import Colis from './services/E-colis'; 
import RendezVous from './services/Rendezvous';
import Modéle from './services/achat';
import Payer from './services/payer';
import {Routes, Route} from 'react-router-dom'
import HeaderPageAcu from './headerPageACU/HeaderPageAcu';
import Consultation from './services/CRUDprofile';
import Modelelettres from './services/ModeleLettre';

function App() {
  return (
    <div className='app'> 
      <Routes>
        <Route path="/service" element={<Header/>} />
        <Route path="/E-lettre" element={<Lettre/>} />
        <Route path="/E-colis" element={<Colis/>} />
        <Route path="/rendez" element={<RendezVous/>} />
        <Route path="/navLettre" element={<Modéle/>} />
        <Route path="/payer" element={<Payer/>} />
        <Route path="/consulter" element={<Consultation/>} />
        <Route path="/" element={<HeaderPageAcu/>} />
        <Route path="/ModeleLettre" element={<Modelelettres/>} />


      </Routes>
    </div>
   
  );
}

export default App;
